(function ( $ ) {

    $.fn.snazzy = function( options ) {

      var settings = $.extend({
          delay: 5
      }, options );

      function init($elements){
        loop_through_elements($elements);
        $(window).on('scroll', function(e){ loop_through_elements($elements); });
        $(window).on('resize', function(e){ loop_through_elements($elements); });
      }

      function loop_through_elements($elements){
        $elements.not($('snazzy_animation')).each(function(){
          checkScrolltop($(this));
        });
      }

      function checkScrolltop($element){
        // console.log($element);
        // console.log('element: ' + $element.offset().top);
        // console.log('window.pageYOffset: ' + window.pageYOffset);
        // console.log('document.body.clientHeight: ' + document.body.clientHeight);
        // console.log('-----------------');
        if(
          ( $element.offset().top > window.pageYOffset && $element.offset().top < $(window).height() ) ||
          $element.offset().top < $(window).height() + window.pageYOffset - $(window).height() / 100 * settings.delay
        ){
          $element.addClass('snazzy_animation');
        }

      }

      init(this);

    };

    $( ".snazzy" ).snazzy();

}( jQuery ));
